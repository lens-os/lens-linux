// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Contains functionality for working with processes, including the current one.
#![no_std]
extern crate lens_syscalls;
extern crate lens_system;

/// Exit the process with status code `status`.
pub fn exit(status: i32) -> ! {
    lens_syscalls::exit(status);
}

/// A process.
pub struct Process {
    pub pid: i32,
}

/// Functionality for launching new processes.
pub mod launch;
