bits 64

section .text
global _start
extern rust_start

_start:
  ; Align stack to 16 bytes.
  xor rbp, rbp
  and rsp, 0xfffffffffffffff0
  ; Start program.
  call rust_start
