use std::process::Command;

fn main() {
    println!("cargo:rustc-link-search=native={}", std::env::current_dir().expect("Couldn't get current directory").display());
    Command::new("nasm").arg("rt0.asm").arg("-f").arg("elf64").arg("-o").arg("rt0.o").output().expect("nasm failed"); // Eat output.
    Command::new("ar").arg("r").arg("librt0.a").arg("rt0.o").output().expect("ar failed");
}
