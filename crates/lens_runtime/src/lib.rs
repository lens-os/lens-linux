#![no_std]
#![feature(alloc_error_handler)]
#![feature(lang_items)]
extern crate rlibc;
use core::alloc::{GlobalAlloc, Layout};

pub struct MmapAllocator;

unsafe impl GlobalAlloc for MmapAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        return lens_syscalls::mmap(0, layout.size(), 0x01 | 0x02, 0x20 | 0x02, -1, 0);
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        lens_syscalls::munmap(ptr, layout.size());
    }
}

#[alloc_error_handler]
fn alloc_error(_layout: Layout) -> ! {
    panic!("Allocation error.");
}

// Can't do cargo tests without libstd.
/*#[cfg(test)]
mod tests {
  #[test]
  fn vec() {
    let mut v = vec!(1, 2, 3); // Allocate.
    assert_eq!(v[0], 1);
    assert_eq!(v[1], 2);
    assert_eq!(v[2], 3);

    v = vec!(4, 5, 6); // Release/reallocate.
    assert_eq!(v[0], 4);
    assert_eq!(v[1], 5);
    assert_eq!(v[2], 6);
  }
}*/

pub mod lang_items;
