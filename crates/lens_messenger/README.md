# `lens_messenger`

The LENS messenger IPC library, a LENS packet flow implemented as a Unix-domain socket. Written for the LENS project.
