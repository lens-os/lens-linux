# `lens_system`

Rust crate for talking to the Linux kernel; specifically, a `struct` for wrapping descriptors so they're closed on drop, an error `struct` for wrapping `errno` values, some other error-handling support, and optionally `no_std` support code from the LENS `runtime` crate. Written for the LENS project.
