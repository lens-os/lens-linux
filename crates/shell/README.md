# shell

The LENS shell. Will only run on a Linux `x86_64` system.

## Building

To build, run the `./build.sh` script. You must have a nightly Rust toolchain, because several parts of LENS use `#![feature(...)]` attributes.

## Running

To run, run the executable Cargo generates at `target/debug/shell`. You will see a `lens>` prompt. Type an absolute path to an executable and `shell` will run it. It cannot resolve `$PATH`, pass arguments to the executable (spaces in the command line are considered part of the executable path), or wait for the child process; it writes the prompt again while the child is still running, and might steal `stdin` data from it. When done, type `exit` to terminate the shell.