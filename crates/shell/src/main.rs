// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]
#![no_main]

extern crate lens_files;
extern crate lens_flows;
extern crate lens_inout;
extern crate lens_process;

#[no_mangle]
fn main() {
    let stdout = lens_inout::stdout::get().expect("Failed to get standard output");
    let stdin = lens_inout::stdin::lines().expect("Failed to get standard input");
    stdout
        .as_ref()
        .send("LENS shell")
        .expect("Failed to write banner");
    loop {
        stdout
            .as_ref()
            .send("lens>")
            .expect("Failed to print prompt");
        let mut read = stdin.as_ref().receive(128).expect("Failed to read command");
        let read_len = read.len();
        let read =
            core::str::from_utf8(&mut read[..read_len]).expect("Failed to validate line as UTF-8");
        let name = &read[..];
        if name == "exit" {
            break;
        }
        let template = lens_process::launch::ProcessTemplate::new_basic(read);
        template.run().expect("Failed to run process");
    }
}
