#![no_std]

#[macro_use]
extern crate syscalls;

use core::ffi::c_void;

#[repr(C)]
pub struct sockaddr {
    pub sa_family: u16,
    pub sa_data: [u8; 14],
}

#[repr(C)]
pub struct sockaddr_un {
    pub sun_family: u16,
    pub sun_path: [u8; 108],
}

#[repr(C)]
pub struct iovec {
    pub iov_base: *mut c_void,
    pub iov_len: usize,
}

#[repr(C)]
pub struct msghdr {
    pub msg_name: *mut c_void,
    pub msg_namelen: i32,
    pub msg_iov: *mut iovec,
    pub msg_iovlen: usize,
    pub msg_control: *mut c_void,
    pub msg_controllen: usize,
    pub msg_flags: u32,
}

#[repr(C)]
pub struct statx_timestamp {
    pub tv_sec: i64,
    pub tv_nsec: u32,
}

#[repr(C)]
pub struct statx_data {
    pub stx_mask: u32,
    pub stx_blksize: u32,
    pub stx_attributes: u64,
    pub stx_nlink: u32,
    pub stx_uid: u32,
    pub stx_gid: u32,
    pub stx_mode: u16,
    pub __spare1: u16,
    pub stx_ino: u64,
    pub stx_size: u64,
    pub stx_blocks: u64,
    pub stx_attributes_mask: u64,
    pub stx_atime: statx_timestamp,
    pub stx_btime: statx_timestamp,
    pub stx_ctime: statx_timestamp,
    pub stx_mtime: statx_timestamp,
    pub stx_rdev_major: u32,
    pub stx_rdev_minor: u32,
    pub stx_dev_major: u32,
    pub stx_dev_minor: u32,
    pub __spare2: [u64; 14],
}

// Macro to generate system call wrappers. TODO: Use Result<$st, i64> to distinguish errno from success.
macro_rules! wrapper {
    ($wn:ident ( ) -> $st:ty = $sn:ident) => {
        pub fn $wn() -> $st {
            match unsafe {
                syscall!($sn)
            } {
                Ok(res) => res as $st,
                Err(errno) => -errno as $st
            }
        }
    };
    ($wn:ident ( $($pn:ident : $pt:ty),* ) -> $st:ty = $sn:ident) => {
        pub fn $wn($($pn: $pt),*) -> $st {
            match unsafe {
                syscall!($sn, $($pn),*)
            } {
                Ok(res) => res as $st,
                Err(errno) => -errno as $st
            }
        }
    };
}

wrapper!(read(fd: i32, buf: *mut u8, count: usize) -> usize = SYS_read);
wrapper!(write(fd: i32, buf: *const u8, count: usize) -> usize = SYS_write);
wrapper!(open(filename: *const u8, flags: i32, mode: u16) -> i32 = SYS_open);
wrapper!(close(fd: i32) -> i32 = SYS_close);
wrapper!(ioctl(fd: i32, request: i32, argp: *mut c_void) -> i32 = SYS_ioctl);
wrapper!(statx(fd: i32, path: *const u8, flags: i32, mask: i32, statxbuf: *mut statx_data) -> i32 = SYS_statx);
wrapper!(mmap(addr: u64, len: usize, prot: i32, flags: i32, fd: i32, off: usize) -> *mut u8 = SYS_mmap);
wrapper!(munmap(addr: *mut u8, size: usize) -> i32 = SYS_munmap);
wrapper!(socket(family: i32, socktype: i32, protocol: i32) -> i32 = SYS_socket);
wrapper!(connect(fd: i32, addr: *mut sockaddr, addrlen: usize) -> i32 = SYS_connect);
wrapper!(accept(fd: i32, peer_addr: *mut sockaddr, peer_addrlen: usize) -> i32 = SYS_accept);
wrapper!(sendmsg(fd: i32, msg: *mut msghdr, flags: u32) -> i32 = SYS_sendmsg);
wrapper!(recvmsg(fd: i32, msg: *mut msghdr, flags: u32) -> isize = SYS_recvmsg);
wrapper!(shutdown(fd: i32, how: i32) -> i32 = SYS_shutdown);
wrapper!(bind(fd: i32, myaddr: *mut sockaddr, addrlen: usize) -> i32 = SYS_bind);
wrapper!(listen(fd: i32, backlog: i32) -> i32 = SYS_listen);
wrapper!(getsockopt(fd: i32, level: i32, optname: i32, optval: *mut c_void, optlen: *mut c_void) -> i32 = SYS_getsockopt);
wrapper!(setsockopt(fd: i32, level: i32, optname: i32, optval: *mut c_void, optlen: usize) -> i32 = SYS_setsockopt);
wrapper!(fork() -> i32 = SYS_fork);
wrapper!(execve(filename: *const u8, argv: *const *const u8, envp: *const *const u8) -> i32 = SYS_execve);
wrapper!(dup(oldfd: i32) -> i32 = SYS_dup);
wrapper!(dup3(oldfd: i32, newfd: i32, flags: i32) -> i32 = SYS_dup3);

#[allow(unused_must_use)]
pub fn exit(error_code: i32) -> ! {
    unsafe {
        syscall!(SYS_exit, error_code);
    }
    loop {} // To convince the compiler that we're serious about -> !
}

#[cfg(test)]
mod tests {
    #[test]
    fn write() {
        super::write(1, b"Hello, world!\n\0" as *const u8, 15); // Cargo test doesn't let us test for stdout.
    }
}
