// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::{byte, object, packet, EndOfFlowError};
use alloc::boxed::Box;
use alloc::string::ToString;
use alloc::vec::Vec;
use core::convert::TryInto;
use lens_system::Result;

/// An implementation of `object::Send` that uses `Display` to render human-readable descriptions.
pub struct DisplaySendFilter {
    pub flow: Box<dyn packet::Send>,
}

impl DisplaySendFilter {
    /// Create a `DisplaySendFilter` that wraps the given `flow`.
    pub fn new(flow: Box<dyn packet::Send>) -> DisplaySendFilter {
        DisplaySendFilter { flow }
    }
}

impl<T: ToString> object::Send<T> for DisplaySendFilter {
    fn send(&self, object: T) -> Result<()> {
        let mut str = object.to_string();
        let str = unsafe { str.as_bytes_mut() };
        match self.flow.send(str) {
            Ok(()) => Ok(()),
            Err(err) => Err(err),
        }
    }
}

/// An implementation of `packet::Send` that writes one line for each packet to an underlying byte flow.
pub struct LineSendFilter {
    pub flow: Box<dyn byte::Write>,
}

impl LineSendFilter {
    /// Create a `LineSendFilter` that wraps the given `flow`.
    pub fn new(flow: Box<dyn byte::Write>) -> LineSendFilter {
        LineSendFilter { flow }
    }
}

impl packet::Send for LineSendFilter {
    fn send(&self, packet: &mut [u8]) -> Result<()> {
        let mut line: Vec<u8> = packet.to_vec();
        line.push(b'\n');
        match self.flow.write(&line[..]) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}

/// An implementation of `packet::Send` that frames packets with leading 64-bit lengths and writes them to an underlying byte flow.
pub struct FramedSendFilter {
    pub flow: Box<dyn byte::Write>,
}

impl FramedSendFilter {
    /// Create a `FramedSendFilter` that wraps the given `flow`.
    pub fn new(flow: Box<dyn byte::Write>) -> FramedSendFilter {
        FramedSendFilter { flow }
    }
}

impl packet::Send for FramedSendFilter {
    fn send(&self, packet: &mut [u8]) -> Result<()> {
        let header = (packet.len() as u64).to_be_bytes();
        let mut frame = header.to_vec();
        frame.extend_from_slice(packet);
        match self.flow.write(&frame[..]) {
            Ok(_) => Ok(()),
            Err(err) => Err(err),
        }
    }
}

/// An implementation of `packet::Receive` that pairs with `FramedSendFilter`.
pub struct FramedReceiveFilter {
    pub flow: Box<dyn byte::Read>,
}

impl FramedReceiveFilter {
    /// Create a `FramedReceiveFilter` that wraps the given `flow`.
    pub fn new(flow: Box<dyn byte::Read>) -> FramedReceiveFilter {
        FramedReceiveFilter { flow }
    }
}

impl packet::Receive for FramedReceiveFilter {
    fn receive(&self, _max_size: usize) -> Result<Vec<u8>> {
        let len = match self.flow.read(8) {
            Ok(v) => {
                if v.len() != 8 {
                    return Err(Box::new(EndOfFlowError()));
                }
                u64::from_be_bytes(v[0..7].try_into().expect("frame header incomplete"))
            }
            Err(err) => {
                return Err(err);
            }
        };
        // TODO: Enforce max_size.
        return self.flow.read(len as usize);
    }
}

/// An implementation of `packet::Receive` that pairs with `LineSendFilter`.
pub struct LineReceiveFilter {
    pub flow: Box<dyn byte::Read>,
    //    pub buffer: Vec<u8>,
}

impl LineReceiveFilter {
    /// Create a `LineReceiveFilter` that wraps the given `flow`.
    pub fn new(flow: Box<dyn byte::Read>) -> LineReceiveFilter {
        LineReceiveFilter {
            flow: flow, /*, buffer: Vec::<u8>::new()*/
        }
    }
}

impl packet::Receive for LineReceiveFilter {
    fn receive(&self, max_size: usize) -> Result<Vec<u8>> {
        let mut buf = Vec::<u8>::with_capacity(max_size);
        loop {
            if buf.len() >= max_size {
                // TODO: Error?
            }
            let mut read_buf = self.flow.read(1)?;
            if read_buf[0] == b'\n' {
                buf.shrink_to_fit();
                return Ok(buf);
            }
            buf.append(&mut read_buf);
        }
    }
}
