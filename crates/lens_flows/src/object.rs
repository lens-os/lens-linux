// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use lens_system::Result;

/// A flow through which objects can be sent.
pub trait Send<T> {
    /// Send the `object` to the flow.
    fn send(&self, object: T) -> Result<()>;
}

/// A flow through which objects can be received.
pub trait Receive<T> {
    /// Receive an object from the flow and return it.
    fn receive(&self) -> Result<T>;
}

// Null object flows are too unsafe to implement because receive can't know what values are safe to return.
