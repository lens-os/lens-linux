// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Contains functionality for working with processes, including the current one.

#![no_std]

extern crate alloc;
extern crate lens_system;

use lens_system::Error;

#[derive(Debug)]
struct EndOfFlowError();

impl Error for EndOfFlowError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl core::fmt::Display for EndOfFlowError {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "end of flow")
    }
}

/// Byte-based flows.
pub mod byte;

/// Packet-based flows.
pub mod packet;

/// Rust object-based flows.
pub mod object;

/// Provides simple filters between different types of flows.
pub mod filters;
