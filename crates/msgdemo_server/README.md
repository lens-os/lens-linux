# `msgdemo_server`

The server side of a small demonstration of LENS messengers (`lens_messenger`) and object flows (`lens_flows` and `lens_postcardflows`). The msgdemo_server program creates a messenger server at `/tmp/lens-msgserver`, receives a `Widget` structure from it, and prints fields from it.
