# Contributing to LENS

There are several ways to contribute to LENS. Perhaps the easiest, and in some ways the most important, is critique and suggestions on LENS' design. While your criticism or suggestion will not be automatically accepted, it will be appreciated and seriously considered. Also appreciated are code contributions, whether to fix bugs, implement features, improve general quality, or otherwise. The project is not currently soliciting other kinds of contribution, but feel free to offer if you so wish.

## Comments, criticism, and suggestions

There are several ways to send these in, and different ones will likely be preferred by different people.

### GitLab issues

If you have a GitLab.com account or don't mind setting one up, you can submit an issue, or a comment on an existing issue, in the appropriate project (at the time of writing, this will always be `lens-linux`). If you use another means of delivery, your message or messages will probably be copied into issues or issue comments (with proper credit) to keep everything in one place.

### Email

If you prefer, you may send mail to `public00178@alm.website` (this address may be replaced if it receives spam). It will probably be read within a day or so.

### ActivityPub social networking

If you use ActivityPub social networking software (aka the "Fediverse"), such as Mastodon, Pleroma, or many others, you may send a note (whether mention or direct message) to `@alexbuzzbee@fosstodon.org`.

## Code

There are also several ways to submit code contributions. In any case, please make sure to include some description of the changes you made and why, at least at a high level.

If you make code changes, please make sure to build LENS with them and test the modified components. If you need or want help at any point in the contribution process, get in touch by email or ActivityPub as described above.

### GitLab merge requests

If you have a GitLab.com account or don't mind setting one up, you can fork the appropriate repository (at the time of writing, this will always be `lens-linux`), make changes in a branch off `trunk` in your fork, and submit a GitLab merge request back to `trunk`. While this is in some ways convenient, it does require an account, and many object to the centralization effect it can produce. Don't hesitate to use one of the other methods listed below if you prefer.

### Pull requests

After cloning the appropriate repository (again, probably `lens-linux`), branching off `trunk`, making your changes, and pushing them to another publically-accessible Git server, you can use [`git request-pull`](https://www.git-scm.com/docs/git-request-pull) to generate a form-letter request to pull your changes from there. The output text it generates can be copy-pasted or redirected directly into an email body or email attachment and mailed to the same email address as given above for commentary and suggestions.

A basic example of using `git request-pull`:

    $ git branch my-change
    $ # ... changes are made ...
	$ git add -A
	$ git commit -m "Make my change"
	$ git push git@git.example.net:lens-linux my-change
	$ git request-pull origin/trunk https://git.example.net/lens-linux my-change | mail public00178@alm.website

While this option is more convenient when you have a public Git server, setting one up if you don't already have one is not easy. The last option should be workable for anyone, though it can be more complicated.

### Mailed patches

After cloning the appropriate repository (again, probably `lens-linux`), branching off `trunk`, and making your changes, you can use [`git format-patch`](https://git-scm.com/docs/git-format-patch) to create patch files containing your changes for sending via email. The files that `git format-patch` generates can be sent directly (using [`git send-email`](https://git-scm.com/docs/git-send-email) or by copying them into a compatible mail client's local Drafts mailbox) or as attachments. Mail your patches to the same email address given above for commentary and suggestions.

A basic example of using `git format-patch`:

	$ git branch my-change
	$ # ... changes are made ...
	$ git add -A
	$ git commit -m "Make my change"
	$ git format-patch origin/trunk -o patches -MC --cover-letter
	$ # ... the files created in the patches directory are attached to an email explaining the changes and sent
